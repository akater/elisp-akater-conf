;;;  -*- lexical-binding: t -*-

(defconst-with-prefix fakemake
  feature 'akater-conf
  authors "Dima Akater"
  first-publication-year-as-string "2020"
  org-files-in-order '("akater-conf-core"
                       "akater-conf")
  org-files-for-testing-in-order '("akater-conf-tests")
  site-lisp-config-prefix "50"
  license "GPL-3")
