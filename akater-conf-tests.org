# -*- coding: utf-8; mode: org-development-elisp; -*-
#+title: akater-conf-tests
#+subtitle: Tests for =akater-conf= Emacs Lisp package
#+author: =#<PERSON akater A24961DE3ADD04E057ADCF4599555CE6F2E1B21D>=
#+property: header-args :tangle (if (memq 'test ob-flags) "akater-conf-tests.el" "") :lexical t
#+startup: nologdone show2levels
#+todo: TODO(t@) HOLD(h@/!) | DONE(d@)
#+todo: BUG(b@/!) | FIXED(x@)
#+todo: TEST(u) TEST-FAILED(f) | TEST-PASSED(p)
#+todo: DEPRECATED(r@) | OBSOLETE(o@)

* Dependencies
#+begin_src elisp :results none
(require 'ort)
(require 'org-src-elisp-extras)
#+end_src

* run
** Definition
#+begin_src elisp :results none
(defun akater-conf-tests-run (&optional directory-with-code)
  "Running this in directory with org files for `akater-conf' package will test the package.

Code (compiled or not) is presumed to be contained in DIRECTORY-WITH-CODE which defaults to “.”.  Relative names are resolved with respect to `default-directory'.

If code is not found in DIRECTORY-WITH-CODE, it is looked up in the rest of `load-path'."
  (setq directory-with-code
        (file-name-as-directory
         (expand-file-name (or directory-with-code "."))))
  (let ((local-load-path (cons directory-with-code load-path)))
    (let (org-confirm-babel-evaluate)
    
      ;; akater-conf-core

      ;; Code to be tested:
      (let ((load-path local-load-path))
        (load "akater-conf-core"))

      ;; Org file with tests:
      (ort-run-tests-in-file "./akater-conf-core.org"
        :save-buffer t))))
#+end_src
